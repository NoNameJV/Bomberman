# Install 

- Setup nginx for serving.
- npm install NodeJS server.

# Nginx configuration 

```
worker_processes  1;

events {
    worker_connections  1024;
}

http {
    default_type  application/octet-stream;
    include  mime.types;
    sendfile  on;
    keepalive_timeout  30;

    server {
        listen 3500;
        root E:/NoNameJV/Bomber; # Update this path on your computer
        server_name localhost;

        location / {
            index client/index.html;
        }

        location /static/ {
            try_files $uri /static;
        }

        location ~ \.css {
            add_header Content-Type text/css;
        }

        location ~ \.js {
            add_header Content-Type application/x-javascript;
        }

    }

}
```

# Roadmap 
 
### Done 

- [x] Server: Création d'un serveur socket.io
- [x] Client: Se connecter au serveur socket.io 
- [x] Client: Charger phaser. 
- [x] Server: Envoyer la carte (Uniquement le nom).
- [x] Client: Afficher la carte. 
- [x] Class pour découper en coordonnée les layers. (début).
- [x] Client: Joueur ou Spectateur ?
- [x] Server: Init Game
- [x] Server: Init player as spectator or player
- [x] Server: Générer un spawn et l'envoyer.
- [x] Client: Générer le sprite.
- [x] Client: Gérer l'état du jeu (start,stop etc..).
- [x] Client: Demande de mouvement.
- [x] Client: Appliquer le mouvement.
- [x] Server: Gérer la demande de mouvement (player et spectateur) et répondre.
- [x] Synchroniser les mouvements pour les spectateurs.
- [x] Gérer les breaks points (start - end).
- [x] Client: Refonte des menus.
- [x] Client: Caméra qui suit le joueur 
- [x] Client: Limiter les mouvements à 1 par seconde.
- [x] Client: Agrandir le jeu.
- [x] Server: Limiter les actions (rate-limit).
- [x] Server: Améliorer les conditions de fin / début (ainsi que le respawn constant).
- [x] Server: Refaire les collections avec Map proprement.
- [x] Server: Gérer la deconnexion des joueurs.
- [x] Server: Améliorer l'évènement PlayerName (regex de vérification).
- [x] Server: Création de la bombe dans la game loop.
- [x] Server: Améliorer et sécurisé le spawn.
- [x] Server: Corriger la restriction des mouvements (bug).

### Todo 

- Server: Mapper la destruction d'une tile. (event DestroyTile par exemple).
- Server: Calculer le champ d'explosion de la bombe à sa création.
- Server: Création d'un tableau (shifted) pour les explosions (ou une class).
- Client: Coder les explosions.
- Client: Lisser les mouvements (lerp).
- Client: Pauser une bombe. (graphiquement parlant).
- Client: Détruire un tile.
- Server: Coder les conditions de mort.
- Server: Gestion d'un scoreboard + meilleur conditions d'auto-respawn.


> Amélioration cosmétique / fonctionelle du jeu.