document.addEventListener("DOMContentLoaded", function(event) {

    var playername_section  = document.getElementById("playername");
    var type_section        = document.getElementById("type");
    var waiting_section     = document.getElementById("waiting");
    var inputElement        = document.getElementById("input_playername");
    var playername_error    = document.getElementById("error_playername");

    function echoError(errMessage) {
        console.log('EchoError called!');
        inputElement.value = '';
        playername_error.innerHTML = errMessage;
        playername_error.style.opacity = '1';
        setTimeout( function() {
            playername_error.style.opacity = '0';
        },4500);
    }

    inputElement.addEventListener('keydown',function (event) {
        if(event.key === 'Enter' || event.key === 'Space') {
            var name = inputElement.value;
            name = name.trim(); 
            console.log(name);
            if(name != "") {
                currentPlayerName = name;
                socket.emit('PlayerName',name);
            }
            else{
                echoError('Please enter a valid name !');
            }
        }
    });

    socket.on('PlayerName',function(data) {
        if(data.state) {
            // Go next!
            playername_section.style.display = 'none';
            type_section.style.display = 'flex';

            var btn_player = document.getElementById('btn_player');
            var btn_spectator = document.getElementById('btn_spectator');

            btn_player.addEventListener('click',function(event) {
                socket.emit('Join',{
                    type: 'player'
                });
            });

            btn_spectator.addEventListener('click',function(event) {
                socket.emit('Join',{
                    type: 'spectator'
                });
            });
        }
        else {
            echoError(data.error);
        }
    });

    socket.on('JoinResponse', function(data) {
        if (data.state === true) {
            type_section.style.display = 'none';
            if(!data.started) {
                waiting_section.style.display = 'flex';
            }
            else {
                document.getElementsByTagName('canvas')[0].style.opacity = '1';
            }
        } 
        else {
            console.log('Game full ! Choose spectator');
        }
    });

});