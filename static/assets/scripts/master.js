var socket = io('http://localhost:3000');
var game;
var playersMap = new Map();
var currentPlayerName;

var mapEntity;
var crateLayer;

document.addEventListener("DOMContentLoaded", function(event) {
    var tileMap;
    var tileSet;

    var gameStarted = false;

    var waiting_section     = document.getElementById("waiting");

    socket.on('InitMap', function(data) {
        tileMap = data.map;
        tileSet = data.tileset;
        game = new Phaser.Game(500, 500, Phaser.AUTO, '', {
            preload: preload,
            create: create,
            update: update
        });
    });

    socket.on('GameDefaultState',function(started) {
        gameStarted = started;
    });

    socket.on('GameStart', function(data) {
        gameStarted = true;
        console.log('Game started');
        waiting_section.style.display = 'none';
        document.getElementsByTagName('canvas')[0].style.opacity = '1';
    });

    socket.on('GameStop', function() {
        gameStarted = false;
        console.log('Game stoped');
        playersMap.forEach( (player,key) => {
            player.destroy();
            playersMap.delete(key);
        });
        waiting_section.style.display = 'flex';
        document.getElementsByTagName('canvas')[0].style.opacity = '0';
    });

    socket.on('PlayerMove', function(player) {
        playersMap.get(player.name).x = player.position.x * 16;
        playersMap.get(player.name).y = player.position.y * 16;
    });

    function addPlayer(name,position) {
        playersMap.set(name,game.add.sprite(position.x * 16, position.y * 16, 'player'));
        let local_player = playersMap.get(name);
        local_player.frame = 1;
        local_player.animations.add('left', [10, 11, 12, 13], 10, true);
        local_player.animations.add('right', [4, 5, 6, 7], 10, true);
        local_player.animations.add('top', [0, 8, 9], 10, true);
        local_player.animations.add('bottom', [1, 2, 3], 10, true);
    }

    socket.on('SelfSpawn', function(spawnPosition) {
        console.log('SelfSpawn triggered');
        addPlayer(currentPlayerName,spawnPosition);
        game.camera.follow(playersMap.get(currentPlayerName), Phaser.Camera.FOLLOW_LOCKON, 0.1, 0.1);
    });

    socket.on('PlayerSpawn', function(player) {
        console.log('PlayerSpawn triggered for '+player.name);
        addPlayer(player.name,player.position);
    });

    socket.on('PlayerDisconnect',function(player) {
        console.log(`${player.name} disconnected from the game`);
        var player = playersMap.get(player.name); 
        player.destroy();
        playersMap.delete(player.name);
    });

    function preload() {
        game.load.tilemap('bomber', '/static/maps/' + tileMap + '.json', null, Phaser.Tilemap.TILED_JSON);
        game.load.image('tileset', '/static/assets/tilesets/' + tileSet);
        game.load.spritesheet('player', '/static/assets/sprites/player_spritesheet_avec_gauche.png', 16, 16);
    }

    function create() {
        mapEntity = game.add.tilemap('bomber');
        mapEntity.addTilesetImage(tileMap, 'tileset');
        mapEntity.createLayer('Plateau');
        mapEntity.createLayer('Mur');
        crateLayer = mapEntity.createLayer('Crate');
        game.renderer.resize(240, 240);
        game.scale.pageAlignHorizontally = true;
        game.scale.pageAlignVertically = true;

        game.stage.backgroundColor = 'transparent';
        game.scale.refresh();
        cursors = game.input.keyboard.createCursorKeys();

        Object.getOwnPropertyNames(cursors)
            .forEach(direction => {
                const cursor = cursors[direction];

                cursor.onDown.add(function () {
                    this.wasJustPressed = true
                }, cursor)
                cursor.onUp.add(function () {
                    this.wasJustPressed = false
                }, cursor)
                cursor.onHoldCallback = function() {
                    cursor.wasJustPressed = false
                }
            })
    }

    function update() {
        if(!gameStarted) return; 
        
        var direction;
        if (cursors.left.wasJustPressed) {
            direction = "left";
        } else if (cursors.right.wasJustPressed) {
            direction = "right";
        } else if (cursors.up.wasJustPressed) {
            direction = "top";
        } else if (cursors.down.wasJustPressed) {
            direction = "bottom";
        } 

        if(direction) {
            //playersMap.get(currentPlayerName).animations.play(direction);
            socket.emit('Action', {
                type: 'Move',
                direction
            });
        }
        else {
            //playersMap.get(currentPlayerName).animations.stop();
        }
    }

});