var ListMap_players     = new Map();
var ListMap_spectators  = new Map();

document.addEventListener("DOMContentLoaded", function(event) {

    var ULElement_players     = document.getElementById('list_players');
    var ULElement_spectators  = document.getElementById('list_spectators');

    function deleteLiElement(id) {
        var liElement = document.getElementById(id);
        liElement && liElement.parentNode.removeChild(liElement);
    }

    socket.on('StatSpectator', function(name) {
        var liElement = document.createElement('li');
        var id = 'ls_'+name;
        liElement.innerHTML = name;
        liElement.id = id;
        ListMap_spectators.set(name,id);
        ULElement_spectators.appendChild(liElement);
    });

    socket.on('StatPlayer', function(name) {
        var liElement = document.createElement('li');
        var id = 'lp_'+name;
        liElement.innerHTML = name;
        liElement.id = id;
        ListMap_players.set(name,id);
        ULElement_players.appendChild(liElement);
    });

    socket.on('StatDisconnect', function(data) {
        if(data.ingame) {
            let id = ListMap_players.get(data.name);
            deleteLiElement(id);
            ListMap_players.delete(data.name);
        }
        else {
            let id = ListMap_spectators.get(data.name);
            deleteLiElement(id);
            ListMap_spectators.delete(data.name);
        }
    });

});