const fs = require('fs');
class PhaserMap {

    /*
        Get phasers layers data and split into Y and X proper positions.
    */
    constructor(json) {
        this.layers = {};
        this.width = json.width;
        this.height = json.height;
        this.tileset = json.tilesets[0].image;
        this.name = json.tilesets[0].name;
        json.layers.forEach( layer => {
            if(!this.layers.hasOwnProperty(layer.name)) {
                this.layers[layer.name] = {
                    data: []
                };

                let data = this.layers[layer.name].data;
                data.push([]);
                let x = 0,y = 0;
                layer.data.forEach( (tileId, tileIndex) => {
                    if(x >= layer.width) {
                        data.push([]);
                        x = 0;
                        y++;
                    }
                    data[y].push(tileId);
                    x++;
                });
            }
        } );
    }

    // Get tile at position X-Y on specific layerName.
    getTileAt(layerName,x,y) {
        if(this.layers.hasOwnProperty(layerName)) {
            return this.layers[layerName].data[y][x] || undefined;
        }
        return undefined;
    }

    getTilesLocation(layerName,researched_tileId) {
        if(this.layers.hasOwnProperty(layerName)) {
            const data  = this.layers[layerName].data;
            const tiles = [];
            data.forEach( (XCoords,YIndex) => {
                XCoords.forEach( (tileId,XIndex) => {
                    if(researched_tileId instanceof Array) {
                        var i = 0, length = researched_tileId.length;
                        for(;i<length;i++) {
                            if(tileId === researched_tileId[i]) {
                                tiles.push({x: XIndex,y: YIndex,id: tileId});
                                break;
                            }
                        }
                    }
                    else {
                        if(tileId === researched_tileId) {
                            tiles.push({x: XIndex,y: YIndex,id: tileId});
                        }
                    }
                });
            });
            return tiles;
        }
        return undefined;
    }

    getAllTilesExcept(layerName,excepted_tileId) {
        if(this.layers.hasOwnProperty(layerName)) {
            const data  = this.layers[layerName].data;
            const tiles = [];
            data.forEach( (XCoords,YIndex) => {
                XCoords.forEach( (tileId,XIndex) => {
                    if(tileId != excepted_tileId) {
                        tiles.push({x: XIndex,y: YIndex,id: tileId});
                    }
                });
            });
            return tiles;
        }
        return undefined;
    }

}
module.exports = PhaserMap;