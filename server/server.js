const port = 3000;

// Require packages
const io        = require('socket.io')(port); 
const async     = require('async');
const chalk     = require('chalk');
const PhaserMap = require('../static/addons/phaserMap.js'); 
const Game      = require('./core/game.js');

// Require JSON !
const events            = require('./data/events.json');
const validationTable   = require('./data/validationTable.json');
const map               = require('../static/maps/bomberman.json'); 

console.log(`Start socket.io server on port ${chalk.yellow.bold(port)}...`);

// Init Class and default Objets
const Bomber = new Game({
    minPlayer: 2,
    maxPlayer: 4,
    map: new PhaserMap(map),
    events,
    io
});
const UserRegex = /^[a-zA-Z0-9]{1,10}$/;
Bomber.setCollidable("Mur");
Bomber.setCollidable("Crate");

io.on('connection',function(socket) {
    console.log(`New client connected with id => ${chalk.yellow.bold(socket.id)}`);
    // Init player account!
    const player = Bomber.initPlayer(socket);

    /*
        Setup player nickname
        socket.emit('PlayerName','fraxken');
    */
    socket.on(events.playerName, name => {
        let error = 'Username not defined';
        if(typeof name === 'string' && player.name == null) {
            name = name.trim();
            if(UserRegex.test(name)) {
                player.name = name;
                console.log(`${chalk.yellow.bold(socket.id)} is now namned ${chalk.green.bold(name)}`);
                socket.emit(events.playerName,{
                    state: true
                });

                return;
            }
            else {
                error = 'Invalid characters or invalid length detected';
            }
        }
        socket.emit(events.playerName,{
            state: false,
            error
        });
    });

    /*
        When player ask to join the game with an argument type == `spectator` or `player`
        socket.emit('Join',{ type : 'player' });
    */
    socket.on(events.join, data => {
        try {
            const type = data.type.toLowerCase();
            if(!validationTable.joinType.hasOwnProperty(type)) return;
            console.log(`player ${chalk.yellow.bold(socket.id)} asked to join as ${chalk.red.bold(type)}`);

            if(type === 'player') {
                if(!player.name) return;
                if(Bomber.playerInGame < Bomber.maxPlayer || Bomber.availableSpawns.length > 0 && !player.ingame) {
                    player.ingame = true;
                    Bomber.playerInGame++;

                    io.emit(events.stat_player,player.name);
                    
                    if(Bomber.playerInGame >= Bomber.minPlayer && !Bomber.started) {
                        io.emit(events.gameStart);
                        Bomber.emit('start');
                    }
                    else if(Bomber.started) {
                        const position = Bomber.getSpawn();
                        player.position = position;
                        player.originalSpawn = Object.assign({},position);
                        
                        socket.broadcast.emit(events.playerSpawn,{
                            position: player.position,
                            name: player.name
                        });
                        setTimeout(() => {
                            socket.emit(events.gameStart);
                        },100);
                    }

                    socket.emit(events.joinResponse,{
                        state: true,
                        started: Bomber.started
                    });
                }
                else {
                    console.log('Sorry game is full!!');
                    socket.emit(events.joinResponse,{
                        state: false,
                        error: "Game is already full"
                    });
                }
            }
            else if(type === 'spectator' && !player.spectator) {
                if(player.ingame) {
                    this.playerInGame--;

                    if(player.spawn) {
                        Bomber.availableSpawns.push(player.originalSpawn);
                        socket.broadcast.emit('PlayerDisconnect',{
                            name: player.name
                        });
                    }
                    // Stop the game if the minimum of player is not respected!
                    if(Bomber.playerInGame < Bomber.minPlayer && Bomber.started) {
                        Bomber.emit('stop');
                    }
                }
                player.spectator = true;
                io.emit(events.stat_spectator,player.name); // send empty event
                socket.emit(events.joinResponse,{
                    state: true,
                    started: Bomber.started
                });
            }
        }
        catch(err) {
            console.log(err);
        }
    });

    /*
        Broadcast and execute actions sended by Player 
        socket.emit('Action', { type : 'Move' , direction : 'left' });
    */
    socket.on(events.action, data => {
        if(Bomber.started && player.ingame && player.spawn) {
            const type = data.type.toLowerCase(); // move,bomb
            if(!validationTable.action.hasOwnProperty(type)) return;

            // Player Ask to move
            if(type === 'move') {

                // Player already in movement
                if(!player.handleAction) return;

                // Test direction
                const direction = data.direction.toLowerCase(); // top, bottom, left, right
                if(!validationTable.direction.hasOwnProperty(direction)) return; // Verify if direction is valid!

                // Check if we can go on this direction!
                let [go,position] = Bomber.move(player.position,direction);
                console.log(`${chalk.green.bold(player.name)} ask to move on ${chalk.yellow.bold(JSON.stringify(position))} - ${chalk.green.bold(go)}`);
                if(go) {
                    // Send to all player
                    player.position = position;
                    player.handleAction = false;
                    io.emit(events.move,{
                        position,
                        name: player.name
                    });
                }
                else {
                    console.log(chalk.red.bold('Invalid position requested by ')+player.name);
                }
            }
            // Player ask to plant a bomb!
            else if(type === 'bomb') {
                const position = data.position; 
                console.log(`${chalk.green.bold(player.name)} ask to drop bomb at X => ${position.x} and Y => ${position.y}`);
                // Check if the bomb is at the same position! 
                if(position.x === player.position.x && position.y === player.position.y) {
                    if(Bomber.addBomb(position)) {
                        // Emit bomb!
                    }
                }
            }
        }
    });

    socket.on('error', err => {
        console.log(err);
    })

    /*
        When the player / spectator leave the game
    */
    socket.on('disconnect', _ => {
        Bomber.disconnectPlayer(socket);
    });

});