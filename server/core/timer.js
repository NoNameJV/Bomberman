class Timer {

    constructor(delta) {
        this.delta = delta; 
        this.tick = 0;
    }

    walk() {
        if(this.tick >= this.delta) {
            this.tick = 0;
            return true;
        }
        this.tick++;
        return false;
    }

}

module.exports = Timer;