const eventEmitter = require('events');
const chalk = require('chalk');
const Bomb = require('./bomb.js');
const Timer = require('./timer.js');

class ExplosionSet {

    constructor() {
    }

    add(items) {
        if(this.list === undefined) {
            this.list = [];
        }
        if(items instanceof Array) {
            items.foreach( subArr , k => {

            });
        }
    }

    go() {
        if(this.list !== undefined) {

        }
        return undefined;
    }

}

/**
 * null : calculated if not given
 * undefined : not optional
 */
const IGameConstructorOpts = {
    minPlayer: 2,
    maxPlayer: null,
    map: undefined,
    events: undefined
}

const IGameConstructorDefault = {
    playerConnected: 0,
    playerInGame: 0,
    started: false,
    collides: new Map(),
    clients: new Map(),
    bombs: new Set(),
    explosionList: new ExplosionSet()
}

class Game extends eventEmitter {

    /**
     * @param {IGameConstructorOpts} opts
     */
    constructor(opts = {}) {
        super();

        Object.assign(this, IGameConstructorOpts, opts);
        Object.assign(this, IGameConstructorDefault);

        this.spawnPoints = this.map.getTilesLocation('Entity',83);
        this.maxPlayer = opts.maxPlayer || this.minPlayer;
        this.io = opts.io;
        this.moveTimer = new Timer(20);

        this.on('start',() => {
            this.started = true;
            this.availableSpawns = this.spawnPoints.slice(0);

            // Assign all spawn first!
            this.clients.forEach( client => {
                console.log(`Assign default spawn to ${client.name}`);
                const position = this.availableSpawns.pop();
                client.position = position;
                client.originalSpawn = Object.assign({},position);
            }); 

            // Send all spawn to player!
            this.clients.forEach( client => {
                // Send my own spawn !
                console.log(`Player ${client.name} spawn at ${chalk.cyan.bold(JSON.stringify(client.position))}`);
                
                client.spawn = true;
                client.socket.emit(this.events.sendSpawn,client.position);

                // Retrieve all connected player spawn!
                this.clients.forEach( subClient => {
                    if(subClient.socket.id != client.socket.id && subClient.name !== null) {
                        console.log(`Send ${chalk.yellow.bold(subClient.name)} playerSpawn to ${chalk.yellow.bold(client.name)}`);
                        client.socket.emit(this.events.playerSpawn,{
                            position: subClient.position,
                            name: subClient.name
                        });
                    }
                }); 
            }); 

            console.log(chalk.cyan.bold('Game started!'));
        });

        var self = this;

        this.on('stop',() => {
            self.started = false;
            self.io.emit(self.events.gameStop);
            console.log(chalk.cyan.bold('Game stopped!'));
        });

        // Game loop
        setInterval( () => {
            self.update();
        },1000 / 60);
    }

    setCollidable(layerName) {
        console.log(`Set collidable layer => ${layerName}`);
        const tiles = this.map.getAllTilesExcept(layerName,0); 
        if(tiles.length > 0) {
            tiles.forEach( position => {
                let vec = `x:${position.x}y:${position.y}`;
                if(this.collides.has(vec) === false) {
                    this.collides.set(vec,1);
                }
            })
        }
    }

    checkPosition(position) {
        if(position.x > this.map.width || position.x < 0 || position.y > this.map.height || position.y < 0) {
            return false;
        }
        let vec = `x:${position.x}y:${position.y}`;
        let ret = !this.collides.has(vec);
        if(ret) {
            // Check player & sprites position!
        }
        return ret;
    }

    addCollide(position) {
        this.collides.set(`x:${position.x}y:${position.y}`,1);
    }

    remCollide(position) {
        var vec = `x:${position.x}y:${position.y}`;
        if(this.collides.has(vec)) {
            this.collides.delete(vec);
        }
    }

    move(from,to) {
        if(from) {
            let final = Object.assign({},from); 
            if(to === 'right') {
                final.x+=1;
            }
            else if(to === 'left') {
                final.x-=1;
            }
            else if(to === 'top') {
                final.y-=1;
            }
            else if(to === 'bottom') {
                final.y+=1;
            }
            //final.id = this.map.getTileAt(final.x,final.y);
            return [this.checkPosition(final),final];
        }
        return undefined;
    }  

    addBomb(position,playerName) {
        let ok = this.checkPosition(position);
        if(ok) {
            // TODO: Add bomb to collision layer!
            this.bombs.add(new Bomb({
                position
            }));
            this.addCollide(position);
            return true;
        }
        return false;
    }

    initPlayer(socket) {
        this.clients.set(socket.id,{
            socket,
            ingame: false,
            spectator: false,
            originalSpawn: null,
            spawn: false,
            alive: true,
            position: null,
            name: null,
            handleAction: true
        });
        this.playerConnected++;
        const user = this.clients.get(socket.id);

        /* Socket Trigger */
        this.on('stop',() => {
            user.spawn = false;
        });
        
        socket.emit(this.events.gameState,this.started);
        socket.emit(this.events.initMap,{map: this.map.name, tileset: this.map.tileset});

        return user;
    }

    disconnectPlayer(socket) {
        const player = this.clients.get(socket.id);
        if(player === undefined) return;
        if(player.name !== undefined) {
            this.io.emit('StatDisconnect',{
                name: player.name,
                ingame: player.ingame
            });
        }
        if(player.ingame) {
            console.log(`player ${chalk.yellow.bold(player.name)} is now disconnected from the game!`);
            this.playerInGame--;

            if(player.spawn) {
                this.availableSpawns.push(player.originalSpawn);
                socket.broadcast.emit('PlayerDisconnect',{
                    name: player.name
                });
            }
            // Stop the game if the minimum of player is not respected!
            if(this.playerInGame < this.minPlayer && this.started) {
                this.emit('stop');
            }
        }
        this.clients.delete(socket.id);
        this.playerConnected--;
    }

    bombExplode(bomb) {
        // Generate explosion here!
        const pos = bomb.position; 
        // Take calculed arena
        // this.explosionList.add(arena);
    }

    update() {
        if(this.started) {
            if(this.moveTimer.walk()) {
                this.clients.forEach( client => {
                    client.handleAction = true;
                }); 
            }

            const explodeCoords = this.explosionList.go();
            if(explodeCoords !== undefined) {
                
            }

            if(this.bombs.size > 0) {
                for (let bomb of this.bombs.values()) {
                    if(bomb.exploded === false && bomb.active === true) {
                        bomb.tick++;
                        if(bomb.tick === bomb.tickExplode) {
                            this.remCollide(bomb.position);
                            bomb.exploded = true;
                            bombExplode(bomb);
                        }
                    }
                }
            }
            
        }
    }

}

module.exports = Game;