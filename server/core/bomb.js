// Require packages
const eventEmitter = require('events');

// Interfaces
const IBombConstructorOpts = {
    tickExplode: 180,
    radius: 1,
    position: null,
    active: true
}

const IBombConstructorDefault = {
    tick: 0,
    exploded: false
}

// Class
class Bomb extends eventEmitter {

    constructor(opts) {
        super();
        Object.assign(this, IBombConstructorOpts, opts);
        Object.assign(this, IBombConstructorDefault);

        // Calcule sub exploded position!
    }

}

module.exports = Bomb;