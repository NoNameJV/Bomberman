const eventEmitter = require('events');

class Player extends eventEmitter {

    constructor() {
        super();
    }

}

module.exports = Player;